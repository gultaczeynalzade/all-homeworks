import React, { useState } from 'react';

import Button from "./components/Button";
import {Modal} from "./components/Modal";

function App() {

  const [firstModalStatus, setFirstModalStatusStatus] = useState(false);
  const [secondModalStatus, setSecondModalStatusStatus] = useState(false);

  const toggleFirstModal = () => setFirstModalStatusStatus(v => !v);
  const toggleSecondModal = () => setSecondModalStatusStatus(v => !v);




    return (
      <div className="App">
          <Button
              key = {1}
              backgroundColor='#b3382c'
              text='First Modal'
              onClick={toggleFirstModal}
          />
          <Button
              key = {2}
              backgroundColor='#b3382c'
              text='Second Modal'
              onClick={toggleSecondModal}
          />

        {firstModalStatus && (
          <Modal
          header='Do you want to delete this file?'
          closeIcon={true}
          text='Once you delete this file, it won’t be possible to undo this action.
                        Are you sure you want to delete it?'
          close={toggleFirstModal}
          actions={[
          <Button
              key = {1}
              backgroundColor='#b3382c'
              text='Ok'
              onClick={() => alert('second')}
          />,
          <Button
              key = {2}
              backgroundColor='#b3382c'
              text='Cancel'
              onClick={toggleFirstModal}
          />
        ]}
          />
          )}



          {secondModalStatus && (
              <Modal
                  header='Do you want to save this file?'
                  closeIcon={true}
                  text='If you save this file, it will saved.
                        Are you sure you want to save it?'
                  close={toggleSecondModal}
                  actions={[
                      <Button
                          key = {3}
                          backgroundColor='black'
                          text='Ok'
                          onClick={() => alert('second')}
                      />,
                      <Button
                          key = {4}
                          backgroundColor='black'
                          text='Cancel'
                          onClick={toggleFirstModal}
                      />
                  ]}
              />
          )}

          </div>
          )
          }
          export default App;