import React from "react";
import PropTypes from "prop-types";

export const Modal = ({header, closeIcon, actions, text, close}) => {
    return (
        <div className="window">
        <div className="modal">
            <header>
                {header}
                {closeIcon && <p onClick={close} className="close-btn">x</p>}
            </header>
            <p className="modal-text">{text}</p>
            <div className="btns">{actions}</div>
        </div>
        </div>

    )
}