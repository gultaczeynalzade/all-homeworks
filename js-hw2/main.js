let a=+prompt("Enter Number1");
let b=+prompt("Enter Number2");
let operation=prompt("Enter operation")
function Calculator() {
    switch (operation) {
        case '+':
            return a + b
            break;
        case '-':
            return a - b
            break;
        case '*':
            return a * b
            break;
        case '/':
            return a / b
            break;
    }
}
alert(Calculator());

/*Function is a block of code which we can use whenever you need and wherever you required to avoid repeated code
blocks. A copy of the value inside the argument is placed on the function’s stack. This is nearly always the default
behaviour. A reference to the variable containing the value is passed into the function. Usually it’s done by simply
mapping the parameter inside the function directly to the same spot in RAM as contained the original variable passed
in as an argument. This is called pass-by-reference, some languages don’t even allow this and you needto perform other
tasks to get similar results, most of the languages which do allow this require you decorate at leastthe parameter in
the function definition to show it should be pass-by-reference.
*/
