let tabs = document.querySelector(".tabs");
let contents = document.querySelector(".tabs-content");

tabs.addEventListener('click', (e) => {
        const { target } = e;
        let tabsList = [];
        for(let el of tabs.children) {
                el.classList.remove('active');
                tabsList.push(el);
        }
        target.classList.add('active');
        const indexOfActiveTab = tabsList.indexOf(document.querySelector('.active'));
        for(let el of contents.children) {
                el.classList.remove('act')
        }
        contents.children[indexOfActiveTab].classList.add('act')
});
