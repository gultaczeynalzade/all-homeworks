import React from 'react';
import { FaStar } from 'react-icons/fa';
import { Link } from 'react-router-dom';

export const Products = ({ title, price, id, number, img, btn_fav, btn_card, color }) => {

    return (
        <Link
            className="products-item">
            <img className="product-image" src={img}/>
            <h5>{title}</h5>
            <div className="price row space-between">
                <h3>${price}</h3>
                <h3>{number}</h3>
            </div>
            <div className="row space-between">
                <div className="buttons">
                    <button onClick={btn_card}>Add to cart</button>
                </div>
            </div>
            <h3 style={{color}}
                onClick={btn_fav}><FaStar /></h3>
        </Link>
    )
};
