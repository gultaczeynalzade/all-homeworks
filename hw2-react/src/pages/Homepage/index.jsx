import React, { useState, useEffect } from 'react';

import { postsFetch } from '../../API/fetchAPI';

import { Products } from './Products'
import { Modal } from './Modal'
import { Button } from './Button'

export const Homepage = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        (async () => {
            const data = await postsFetch();
            setPosts(data);
        })()
    }, []);


    // Modal

    const [firstModalStatus, setFirstModalStatusStatus] = useState(false);

    const toggleFirstModal = () => setFirstModalStatusStatus(v => !v);

    const toggleAdd = () => {

        toggleFirstModal();
        alert('added')
    };

    //Modal


    return (
        <div>
            <div className="products">
                {!!posts.length ?
                    posts.map(({ id, name, price, number, img }) => (
                        <Products
                            key={id}
                            img={img}
                            title={name}
                            id={id}
                            price={price}
                            number={number}
                            color='darkred'
                            btn_fav= {() => {alert('added')}}
                            btn_card={toggleFirstModal}
                        />

                    ))
                    :
                    null
                }
            </div>

            {firstModalStatus && (
                <Modal
                    header='Do you want to add this product to card?'
                    closeIcon={true}
                    text='If you click add button, it will be added; otherwise, it will not.'
                    close={toggleFirstModal}
                    actions={[
                        <Button
                            key = {1}
                            backgroundColor='darkred'
                            text='Add'
                            onClick={toggleAdd}
                        />,
                        <Button
                            key = {2}
                            backgroundColor='#b3382c'
                            text='Cancel'
                            onClick={toggleFirstModal}
                        />
                    ]}
                />
            )}
        </div>
    )
};
