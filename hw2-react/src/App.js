import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';

import {
    Homepage,
} from './pages'

function App() {
    return (
        <Router>
            <header className="header">
                <div className="row space-between">
                    <h3 className="store-name">Film Store</h3>
                    <nav className="navigation-links">
                        <NavLink className="nav-list" exact to="/">Home</NavLink>
                        <NavLink className="nav-list" to="/">Contact Us</NavLink>
                        <NavLink className="nav-list" to="/">Shipping Information</NavLink>
                        <NavLink className="nav-list" to="/">Cart</NavLink>
                        <NavLink className="nav-list" to="/">Login</NavLink>
                    </nav>
                </div>
            </header>
            <Switch>
                <Route exact path="/" component={Homepage} />
            </Switch>
        </Router>
    );
}

export default App;
