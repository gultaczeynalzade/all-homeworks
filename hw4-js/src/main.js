function filterBy(arr,arg){
   let arr1 = [];
   arg = "number";
   for(let el of arr){
       if(typeof el!=typeof arg){
           arr1.push(el);
       }
   }
   return arr1;
}
alert(filterBy(['hello','world',23,null,'23']));




//For loop is used to loop through each element of the array.
// forEach() is a method of array that is used to call a particular method for all elements of the array. So if you want
// to perform basic operation on elements of array then use for loop. And also the performance of for loop is great. But
// if you want to use each element of array in any function and want to pass it as a parameter to function then use
// forEach().In for loop you can control how you iterate through each element of array whereas forEach() does iteration
// automatically. So, it also depends on whether you want to use each and every element or conditionally iterate through.